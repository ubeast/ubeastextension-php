<?php

/**
 * Ubeast.ru DayZ Mod server extension script
 * Check if player chat is muted
 * Author: inurosen
 * Date: 29/11/16
 * @var $_config array Current config
 * @param player_uid int Player Steam UID
 */
if(!isset($params))
{
    die('ERROR');
}
if(!isset($params['player_uid']))
{
    die('ERROR');
}

$db_config = $_config['database'];
$mysql = new mysqli($db_config['host'], $db_config['username'], $db_config['password'], $db_config['database'], $db_config['port']);
$p_st = $mysql->prepare('SELECT `ExpireAt`, `Reason` FROM `chat_mute` WHERE `PlayerUID` = ? AND `ExpireAt` > NOW();');
if($p_st === false)
{
    die('ERROR');
}
if(!$p_st->bind_param('s', $params['player_uid']))
{
    die('ERROR');
}
if(!$p_st->execute())
{
    die('ERROR');
}
$db_result = $p_st->get_result();

if($db_result->num_rows == 0)
{
    $p_st->free_result();
    die('0');
}

$result = $db_result->fetch_assoc();
$p_st->free_result();

die(sprintf('["%s", "%s"]', $result['ExpireAt'], $result['Reason']));
