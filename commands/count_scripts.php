<?php
/**
 * Ubeast.ru DayZ Mod server extension script
 * Get custom code script names with their section counts
 * Author: inurosen
 * Date: 29/11/16
 * @var $_config array Current config
 */

$db_config = $_config['database'];
$mysql = new mysqli($db_config['host'], $db_config['username'], $db_config['password'], $db_config['database'], $db_config['port']);
$p_st = $mysql->prepare('SELECT `script_name`, `section_count` AS `count` FROM `ubeast_code` WHERE `enabled` = 1;');
if($p_st === false)
{
    die();
}
if(!$p_st->execute())
{
    die();
}

$db_result = $p_st->get_result()->fetch_all(2);
$p_st->free_result();

die(json_encode($db_result));
