<?php
/**
 * Ubeast.ru DayZ Mod server extension script
 * Execute custom query
 * Author: inurosen
 * Date: 29/11/16
 * @var $_config array Current config
 * @param query string Mysql query
 */
if(!isset($params))
{
    die('ERROR');
}
if(!isset($params['query']))
{
    die('ERROR');
}

$db_config = $_config['database'];
$mysql = new mysqli($db_config['host'], $db_config['username'], $db_config['password'], $db_config['database'], $db_config['port']);
$p_st = $mysql->prepare($params['query']);
if($p_st === false)
{
    die('ERROR');
}
if(!$p_st->execute())
{
    die('ERROR');
}

die((string)$p_st->insert_id);