<?php
/**
 * Ubeast.ru DayZ Mod server extension script
 * Get saved loot
 * Author: inurosen
 * Date: 29/11/16
 * @var $_config array Current config
 * @param player_uid int Player Steam UID
 */
if(!isset($params))
{
    die();
}
if(!isset($params['player_uid']))
{
    die();
}

$db_config = $_config['database'];
$mysql = new mysqli($db_config['host'], $db_config['username'], $db_config['password'], $db_config['database'], $db_config['port']);
$p_st = $mysql->prepare('SELECT `weapons`, `magazines`, `backpacks` FROM `ubeast_saved_loot` WHERE `PlayerUID` = ? AND `status` = 0;');
if($p_st === false)
{
    die();
}
if(!$p_st->bind_param('s', $params['player_uid']))
{
    die();
}
if(!$p_st->execute())
{
    die();
}
$db_result = $p_st->get_result();

if($db_result->num_rows == 0)
{
    $p_st->free_result();
    die();
}

$result = $db_result->fetch_assoc();
$return = sprintf('[%s,%s,%s]', $result['weapons'], $result['magazines'], $result['backpacks']);

$p_st = $mysql->prepare('UPDATE `ubeast_saved_loot` SET `status` = 1 WHERE `PlayerUID` = ? AND `status` = 0;');
if($p_st === false)
{
    die();
}
if(!$p_st->bind_param('s', $params['player_uid']))
{
    die();
}
if(!$p_st->execute())
{
    die();
}

die($return);