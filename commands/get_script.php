<?php

/**
 * Ubeast.ru DayZ Mod server extension script
 * Get script code section
 * Author: inurosen
 * Date: 29/11/16
 * @var $_config array Current config
 * @param script_name string Name of the script
 * @param section int Section number of script
 */
if(!isset($params))
{
    die();
}
if(!isset($params['script_name']))
{
    die();
}
if(!isset($params['section']))
{
    die();
}
if($params['section'] < 1)
{
    die();
}

$db_config = $_config['database'];
$mysql = new mysqli($db_config['host'], $db_config['username'], $db_config['password'], $db_config['database'], $db_config['port']);
$p_st = $mysql->prepare('SELECT `code` FROM `ubeast_code` WHERE `script_name` = ? AND `enabled` = 1 LIMIT 1;');
if($p_st === false)
{
    die();
}
if(!$p_st->bind_param('s', $params['script_name']))
{
    die();
}
if(!$p_st->execute())
{
    die();
}
$db_result = $p_st->get_result();

if($db_result->num_rows != 1)
{
    $p_st->free_result();
    die();
}

$result = $db_result->fetch_assoc();
$p_st->free_result();
$result_array = explode('//--ubeast_section--', $result['code']);
if($params['section'] > count($result_array))
{
    die();
}
$return = $result_array[$params['section'] - 1];

die($return);
