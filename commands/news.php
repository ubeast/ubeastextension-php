<?php

/**
 * Ubeast.ru DayZ Mod server extension script
 * Return news for pause menu
 * Author: inurosen
 * Date: 29/11/16
 * @param lang string Language
 */

if(!isset($params))
{
    die('ERROR');
}
if(!isset($params['lang']))
{
    die('ERROR');
}

$news = [];

switch ($params['lang']) {
    case 'RU':
        $news = [
            "<t size='0.5' align='left' shadow='1'>- Теперь можно перезаряжать Ми-17</t><br />",
            "<t size='0.5' align='left' shadow='1'>- Для бортовых пулеметов доступна новая опция «Добавить запасной магазин». Боеприпасы будут добавлены в запас</t><br />",
            "<t size='0.5' align='left' shadow='1'>- Добавлена возможность самоубийства с помощью пистолета</t><br />",
            "<t size='0.5' align='left' shadow='1'>- Добавлен СВД (НСПУ), который иногда можно найти вместо обычного СВД</t><br />",
        ];
        break;
    default:
        $news = [
            "<t size='0.5' align='left' shadow='1'>- Mi-17 turrets are now can be rearmed</t><br />",
            "<t size='0.5' align='left' shadow='1'>- New Add Extra Ammo option is now available on turrets. Ammo will be added as extra magazine</t><br />",
            "<t size='0.5' align='left' shadow='1'>- Added suicide option on pistols right click menu</t><br />",
            "<t size='0.5' align='left' shadow='1'>- Added SVD (NSPU) which sometimes can be found instead of regular SVD</t><br />",
        ];
        break;
}

$newsEncoded = [];

foreach ($news as $line) {
    $newsEncoded[] = implode(',', utf8OrdString($line));
}

die('[' . implode(',', $newsEncoded) . ']');