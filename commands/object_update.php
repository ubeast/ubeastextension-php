<?php
/**
 * Ubeast.ru DayZ Mod server extension script
 * Update object classname
 * Author: inurosen
 * Date: 29/11/16
 * @var $_config array Current config
 * @param classname string New Classname
 * @param object_uid integer Object UID
 */
if(!isset($params))
{
    die('ERROR');
}
if(!isset($params['fields']))
{
    die('ERROR');
}
if(!empty($params['filters']))
{
    die('ERROR');
}

$db_config = $_config['database'];
$mysql = new mysqli($db_config['host'], $db_config['username'], $db_config['password'], $db_config['database'], $db_config['port']);

$what = [];
foreach ($params['fields'] as $field) {
    $where[] = sprintf('`%s` = \'%s\'', $field['key'], $field['val']);
}

$where = [];
foreach ($params['filters'] as $filter) {
    $where[] = sprintf('`%s` = \'%s\'', $filter['key'], $filter['val']);
}

$p_st = $mysql->prepare(sprintf('UPDATE `object_data` SET %s WHERE %s;', implode(', ', $what), implode(' AND ', $where)));
if($p_st === false)
{
    die('ERROR');
}
if(!$p_st->execute())
{
    die('ERROR');
}

die((string)$p_st->affected_rows);