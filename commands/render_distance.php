<?php

/**
 * Ubeast.ru DayZ Mod server extension script
 * Return available render distance options
 * Author: inurosen
 * Date: 29/11/16
 * @var $_config array Current config
 * @param player_uid int Player Steam UID
 */
if(!isset($params))
{
    die();
}
if(!isset($params['player_uid']))
{
    die();
}

$db_config = $_config['database'];
$mysql = new mysqli($db_config['host'], $db_config['username'], $db_config['password'], $db_config['database'], $db_config['port']);
$p_st = $mysql->prepare('SELECT `RenderDistance` FROM `render_distances` WHERE `PlayerUID` IN(0, ?) AND `ExpireAt` > NOW();');
if($p_st === false)
{
    die();
}
if(!$p_st->bind_param('s', $params['player_uid']))
{
    die();
}
if(!$p_st->execute())
{
    die();
}
$db_result = $p_st->get_result();

if($db_result->num_rows == 0)
{
    $p_st->free_result();
    die('[400,600,800,1000,1300,1600]');
}

$result = $db_result->fetch_all(2);
$return = [];

foreach($result as $row)
{
    $return = array_merge($return, json_decode($row[0], true));
}
$p_st->free_result();

$return = array_unique($return);
sort($return);
die(json_encode(array_values($return), JSON_OBJECT_AS_ARRAY));
