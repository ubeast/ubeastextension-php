<?php

/**
 * Ubeast.ru DayZ Mod server extension script
 * Check if player chat is muted
 * Author: inurosen
 * Date: 29/11/16
 * @var $_config array Current config
 * @param player_uid int Player Steam UID
 */
if(!isset($params))
{
    die('ERROR');
}
if(!isset($params['player_uid']))
{
    die('ERROR');
}

define('CHAT_MUTE', 'CHAT_MUTE');
define('BLOOD_TOTAL', 'BLOOD_TOTAL');

$db_config = $_config['database'];
$mysql = new mysqli($db_config['host'], $db_config['username'], $db_config['password'], $db_config['database'], $db_config['port']);
$p_st = $mysql->prepare('SELECT `Restriction`, `ExpireAt`, `Params`, `Reason` FROM `player_restrictions` WHERE `PlayerUID` = ? AND `ExpireAt` > NOW();');
if($p_st === false)
{
    die('ERROR');
}
if(!$p_st->bind_param('s', $params['player_uid']))
{
    die('ERROR');
}
if(!$p_st->execute())
{
    die('ERROR');
}
$db_result = $p_st->get_result();

$result = [];

while($row = $db_result->fetch_assoc()) {
    $result[] = [ $row['Restriction'], $row['ExpireAt'], $row['Params'], utf8OrdString($row['Reason'])];
}

$p_st->free_result();

die(json_encode($result, JSON_UNESCAPED_UNICODE));
