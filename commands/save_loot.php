<?php
/**
 * Ubeast.ru DayZ Mod server extension script
 * Save loot in box
 * Author: inurosen
 * Date: 29/11/16
 * @var $_config array Current config
 * @param player_uid int Player Steam UID
 * @param cargo array Box cargo [weapons, magazines, backpacks]
 */
if(!isset($params))
{
    die();
}
if(!isset($params['player_uid']))
{
    die();
}
if(!isset($params['cargo']))
{
    die();
}

$db_config = $_config['database'];
$mysql = new mysqli($db_config['host'], $db_config['username'], $db_config['password'], $db_config['database'], $db_config['port']);
$q = 'INSERT INTO `ubeast_saved_loot` (`PlayerUID`, `weapons`, `magazines`, `backpacks`, `status`) VALUES (?, ?, ?, ?, 0)
ON DUPLICATE KEY UPDATE `weapons` = ?, `magazines` = ?, `backpacks` = ?, `status` = 0';
$p_st = $mysql->prepare($q);
if($p_st === false)
{
    die();
}

$weapons = json_encode($params['cargo'][0], JSON_OBJECT_AS_ARRAY);
$magazines = json_encode($params['cargo'][1], JSON_OBJECT_AS_ARRAY);
$backpacks = json_encode($params['cargo'][2], JSON_OBJECT_AS_ARRAY);

if(!$p_st->bind_param('sssssss', $params['player_uid'], $weapons, $magazines, $backpacks, $weapons, $magazines, $backpacks));
{
    die();
}
if(!$p_st->execute())
{
    die();
}

die('OK');