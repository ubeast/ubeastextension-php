<?php
/**
 * Ubeast.ru DayZ Mod server extension router
 * Author: inurosen
 * Date: 29/11/16
 * Usage: php filename.php count_scripts
 */

require_once ('utils.php');

// Change this to your HiveExt.ini path
$hive_ini_path = __DIR__ . '/../../cfgdayz/HiveExt.ini';

// -- DO NOT EDIT BELOW --

$_config = [
    'database' => [
        'host'     => '127.0.0.1',
        'port'     => 3306,
        'database' => 'dayz',
        'username' => 'root',
        'password' => '',
    ],
];

$parsed_config = parse_ini_file($hive_ini_path, true, INI_SCANNER_RAW);

file_put_contents(__DIR__ . '/logs/extension.txt', date('Y-m-d H:i:s') . ' >> ' . print_r($argv, true), FILE_APPEND);

// if for some reason there is no valid
// ini config, we just continue using defaults
if($parsed_config !== false)
{
    if(isset($parsed_config['Database']))
    {
        if(isset($parsed_config['Database']['Host']))
        {
            $_config['database']['host'] = $parsed_config['Database']['Host'];
        }
        if(isset($parsed_config['Database']['Port']))
        {
            $_config['database']['port'] = $parsed_config['Database']['Port'];
        }
        if(isset($parsed_config['Database']['Database']))
        {
            $_config['database']['database'] = $parsed_config['Database']['Database'];
        }
        if(isset($parsed_config['Database']['Username']))
        {
            $_config['database']['username'] = $parsed_config['Database']['Username'];
        }
        if(isset($parsed_config['Database']['Password']))
        {
            $_config['database']['password'] = $parsed_config['Database']['Password'];
        }
    }
}

if(!isset($argv[1]))
{
    die('ERROR');  // No command specified
}
$command = $argv[1];
$command_path = __DIR__ . '/commands/' . $command . '.php';

if(!file_exists($command_path))
{
    die('ERROR');  // Non existing command
}

if(isset($argv[2]))
{
    // echo $argv[2] . PHP_EOL;
    $params_raw = base64_decode($argv[2], true);
    if($params_raw === false)
    {
        die('ERROR');
    }
    // echo $params_raw . PHP_EOL;

    $params = json_decode($params_raw, true);
    if(json_last_error())
    {
        die('ERROR');  // Non parseable params
    }
    // print_r($params);
}

require_once($command_path);  // Execute command