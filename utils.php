<?php

function utf8Ord($char) {
    if (mb_check_encoding($char, 'UTF-8')) {
        $ret = mb_convert_encoding($char, 'UTF-32BE', 'UTF-8');
        return hexdec(bin2hex($ret));
    } else {
        return null;
    }
}

function utf8OrdString($string) {
    $lineEncoded = [];

    for ($i = 0; $i < mb_strlen($string); $i++){
        $lineEncoded[] = (int)utf8Ord(mb_substr($string, $i, 1));
    }

    return $lineEncoded;
}